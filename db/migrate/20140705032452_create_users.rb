class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.integer :client_id
      t.string  :first_name
      t.string  :last_name
      t.string  :type

      t.timestamps
    end
  end
end
