class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.string  :title
      t.date    :date
      t.integer :total

      t.timestamps
    end
  end
end
