class CreateInvoiceItems < ActiveRecord::Migration
  def change
    create_table :invoice_items do |t|
      t.integer :invoice_id
      t.string  :description
      t.integer :hours
      t.integer :rate
      t.integer :amount

      t.timestamps
    end
  end
end
