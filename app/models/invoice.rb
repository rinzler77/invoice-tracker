class Invoice < ActiveRecord::Base
  scope :for_month, ->(time) { where("created_at >= ? AND ? < created_at", time.beginning_of_month, time.end_of_month) } 
end
