class Admin < ActiveRecord::Base
  devise :database_authenticatable, :rememberable, :recoverable, :trackable
end
