class WelcomeController < ApplicationController
  def home
    @invoices = Invoice.for_month(Time.now)
  end
end
