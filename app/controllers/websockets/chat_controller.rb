class Websockets::ChatController < WebsocketRails::BaseController
  def authorize_channel
    channel = message[:channel]

    if current_admin
      accept_channel current_admin
      WebsocketRails[channel].filter_with(Websockets::ChatController, :incoming_message)
    end
  end

  def incoming_message
    channel = event.channel
    message = event.data[:message]
    WebsocketRails[channel].trigger('broadcast', message: message)
  end
end
