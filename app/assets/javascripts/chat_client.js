var ChatClient = {
  dispatcher: '', channel: '', room: '', messages: '', message: '',

  connect:         function(client){
                     return function() {
                       if(client.room.value.length > 0 && !isNaN(client.room.value)) {
                         client.channel = client.dispatcher.subscribe_private('room_' + client.room.value);
                         client.channel.bind('broadcast', client.update_messages(client));
                       }
                     }
                   },

  send_message:    function(client) {
                     return function() {
                       if(client.channel != null) {
                         client.channel.trigger('message', {"message": client.message.value});
                       }
                     }
                   },

  update_messages: function(client) {
                     return function(letter) {
                       client.messages.value += "\n" + letter["message"];
                     }
                   },

  _bind_join_btn:  function() {
                     var join_button = document.getElementById('join-button');
                     join_button.addEventListener('click', this.connect(this));
                   },

  _bind_send_btn:  function() {
                     var send_button = document.getElementById("send-button");
                     send_button.addEventListener('click', this.send_message(this));
                   },

  _var_init:       function(host) {
                     this.dispatcher = new WebSocketRails(host + '/websocket');
                     this.room       = document.getElementById('room');
                     this.messages   = document.getElementById('messages');
                     this.message    = document.getElementById('message');                          
                   },

  init:            function(host) {
                     this._var_init(host);
                     this._bind_join_btn();
                     this._bind_send_btn();
                   },
};

ChatClient.init('localhost:3000');
