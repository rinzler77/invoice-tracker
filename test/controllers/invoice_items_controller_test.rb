require "test_helper"

class InvoiceItemsControllerTest < ActionController::TestCase

  def invoice_item
    @invoice_item ||= invoice_items :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:invoice_items)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference('InvoiceItem.count') do
      post :create, invoice_item: {  }
    end

    assert_redirected_to invoice_item_path(assigns(:invoice_item))
  end

  def test_show
    get :show, id: invoice_item
    assert_response :success
  end

  def test_edit
    get :edit, id: invoice_item
    assert_response :success
  end

  def test_update
    put :update, id: invoice_item, invoice_item: {  }
    assert_redirected_to invoice_item_path(assigns(:invoice_item))
  end

  def test_destroy
    assert_difference('InvoiceItem.count', -1) do
      delete :destroy, id: invoice_item
    end

    assert_redirected_to invoice_items_path
  end
end
