require "test_helper"

class InvoicesControllerTest < ActionController::TestCase

  def invoice
    @invoice ||= invoices :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:invoices)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference('Invoice.count') do
      post :create, invoice: {  }
    end

    assert_redirected_to invoice_path(assigns(:invoice))
  end

  def test_show
    get :show, id: invoice
    assert_response :success
  end

  def test_edit
    get :edit, id: invoice
    assert_response :success
  end

  def test_update
    put :update, id: invoice, invoice: {  }
    assert_redirected_to invoice_path(assigns(:invoice))
  end

  def test_destroy
    assert_difference('Invoice.count', -1) do
      delete :destroy, id: invoice
    end

    assert_redirected_to invoices_path
  end
end
