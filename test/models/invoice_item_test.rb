require "test_helper"

class InvoiceItemTest < ActiveSupport::TestCase

  def invoice_item
    @invoice_item ||= InvoiceItem.new
  end

  def test_valid
    assert invoice_item.valid?
  end

end
