require "test_helper"

class InvoiceTest < ActiveSupport::TestCase

  def invoice
    @invoice ||= Invoice.new
  end

  def test_valid
    assert invoice.valid?
  end

end
