require "test_helper"

class ClientTest < ActiveSupport::TestCase

  def client
    @client ||= Client.new
  end

  def test_valid
    assert client.valid?
  end

end
