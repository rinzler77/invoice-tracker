require "test_helper"

class AccountStatementTest < ActiveSupport::TestCase

  def account_statement
    @account_statement ||= AccountStatement.new
  end

  def test_valid
    assert account_statement.valid?
  end

end
